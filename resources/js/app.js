import './bootstrap';

$(document).on('click', '._confirm_alert', function (e) {
    e.preventDefault()
    let result = confirm('Вы уверены?')
    if (result) {
        location.href = $(this).prop('href') ? $(this).prop('href') : $(this).data('href')
    }
})
