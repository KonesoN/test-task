@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="post">
            <div class="@error('title') was-validated @enderror">
                <label for="title" class="form-label">Название</label>
                <div class="input-group has-validation">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Название" required
                           value="{{old('title')?old('title'):(!empty($item)?$item->title:'')}}">
                    <div class="invalid-feedback">
                        Обязательное поле
                    </div>
                </div>
            </div>
            <hr class="my-3">
            <div class="@error('body') was-validated @enderror">
                <div class="form-floating">
                        <textarea class="form-control" placeholder="Содержимое блога" id="body" name="body"
                                  style="height: 100px"
                                  required>{{old('body')?old('body'):(!empty($item)?$item->body:'')}}</textarea>
                    <label for="body">Содержимое блога</label>
                    <div class="invalid-feedback">
                        Обязательное поле
                    </div>
                </div>
            </div>
            @if(!empty($item))
                <hr class="my-4">
                <img src="{{$item->image_url}}"/>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="is_generate_new_photo"
                           name="is_generate_new_photo">
                    <label class="form-check-label" for="is_generate_new_photo">Заменить фото</label>
                </div>
            @endif
            <hr class="my-4">
            <button class="w-100 btn btn-primary btn-lg" type="submit">@if(!empty($item))
                    Редактировать
                @else
                    Создать
                @endif</button>
        </form>
    </div>
@endsection



