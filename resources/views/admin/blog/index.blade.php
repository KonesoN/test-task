@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="d-flex gap-2 justify-content-center pb-3">
            <a href="{{route('admin.blog.form')}}" class="btn btn-success rounded-pill px-3" type="button">Создать</a>
        </div>
        <div class="table-responsive small">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название</th>
                    <th scope="col">Действие</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pagination->items() as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td>{{$item->title}}</td>
                        <td>
                            <div class="d-flex gap-2">
                                <a href="{{route('admin.blog.id',$item->id)}}" class="btn btn-primary rounded-pill px-3" type="button">Редактировать
                                </a>
                                <a href="{{route('admin.blog.action',[$item->id,'delete'])}}" class="btn btn-danger rounded-pill px-3 _confirm_alert" type="button">Удалить</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @include('partials.pagination.index')
        </div>
    </div>
@endsection
