@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @foreach($pagination->items() as $item)
                <div class="col-md-6">
                    <div
                        class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                        <div class="col p-4 d-flex flex-column position-static">
                            <h3 class="mb-0">{{$item->title}}</h3>
                            <div class="mb-1 text-body-secondary">{{$item->created_at}}</div>
                            <p class="card-text mb-auto">{{substr($item->body,0,100)}}...</p>
                            <a href="{{route('blog.item',$item->id)}}"
                               class="icon-link gap-1 icon-link-hover stretched-link">
                                Читать полностью
                                <svg class="bi">
                                    <use xlink:href="#chevron-right"></use>
                                </svg>
                            </a>
                        </div>
                        <div class="col-auto d-none d-lg-block">
                            <img class="bd-placeholder-img" style="width: 200px; height: 200px; object-fit: cover;"
                                 src="{{$item->image_url}}"/>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        @include('partials.pagination.index')
    </div>
@endsection
