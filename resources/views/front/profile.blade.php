@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="post" enctype="multipart/form-data">
            <div>
                <label for="email" class="form-label">Email</label>
                <div class="input-group has-validation">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required
                           value="{{old('email')?old('email'):Auth::user()->email}}">
                    <div class="invalid-feedback @if($errors->first('email')) d-block @endif">
                        {{$errors->first('email')}}
                    </div>
                </div>
            </div>
            <hr class="my-3">
            <div>
                <label for="password" class="form-label">Пароль</label>
                <div class="input-group has-validation">
                    <input type="password" class="form-control" id="password" name="password" placeholder="password">
                </div>
            </div>
            <hr class="my-3">
            <div>
                <label for="formFile" class="form-label">Выбрать аватар (png, jpg, jpeg)</label>
                <input class="form-control" type="file" id="formFile" name="avatar" accept="image/*">
            </div>
            <hr class="my-4">
            <button class="w-100 btn btn-primary btn-lg" type="submit">Сохранить</button>
        </form>
        <hr class="my-4">
        <a href="{{route('profile.delete')}}" class="w-100 btn btn-danger btn-lg" type="submit">Удалить аккаунт</a>
    </div>
@endsection
