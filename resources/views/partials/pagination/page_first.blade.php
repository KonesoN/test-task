<li class="page-item active">
    <a class="page-link" href="{{route(Route::current()->getName())}}?page=1{{$query_search}}">1</a></li>
@if($pagination->lastPage() >=2)
    <li class="page-item"><a class="page-link" href="{{route(Route::current()->getName())}}?page=2{{$query_search}}">2</a>
    </li>
@endif
@if($pagination->lastPage() >3)
    <li class="page-item"><a class="page-link" href="{{route(Route::current()->getName())}}?page=3{{$query_search}}">3</a>
    </li>
@endif
