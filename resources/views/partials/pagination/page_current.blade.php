@if($pagination->lastPage() > 3)
    <li class="page-item">
        <a class="page-link" href="{{route(Route::current()->getName())}}?page={{$pagination->currentPage() - 2}}{{$query_search}}">{{$pagination->currentPage() - 2}}</a>
    </li>
@endif
<li class="page-item">
    <a class="page-link" href="{{route(Route::current()->getName())}}?page={{$pagination->currentPage() - 1}}{{$query_search}}">{{$pagination->currentPage() - 1}}</a>
</li>
<li class="page-item active">
    <a class="page-link" href="{{route(Route::current()->getName())}}?page={{$pagination->currentPage()}}{{$query_search}}">{{$pagination->currentPage()}}</a>
</li>
