@if($pagination->lastPage() >= $pagination->currentPage() - 1)
    <li class="page-item">
        <a class="page-link" href="{{route(Route::current()->getName())}}?page={{$pagination->currentPage() - 1}}{{$query_search}}">{{$pagination->currentPage() - 1}}</a>
    </li>
@endif
@if($pagination->lastPage() >=2)
    <li class="page-item active">
        <a class="page-link" href="{{route(Route::current()->getName())}}?page={{$pagination->currentPage()}}{{$query_search}}">{{$pagination->currentPage()}}</a>
    </li>
@endif
@if($pagination->lastPage() >= $pagination->currentPage() + 1)
    <li class="page-item">
        <a class="page-link" href="{{route(Route::current()->getName())}}?page={{$pagination->currentPage() + 1}}{{$query_search}}">{{$pagination->currentPage() + 1}}</a>
    </li>
@endif
