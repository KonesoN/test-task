@php
    $query_search = '';
    $count = 0;
    foreach (!empty($filters)?$filters:[] as $id => $value) {
        $query_search .= '&'.$id . '=' . $value;
        $count++;
    }
@endphp
@if($pagination->currentPage() > 1 || $pagination->hasMorePages())
    <nav aria-label="Page navigation example" style="margin: 0 auto;">
        <ul class="pagination" style="justify-content: center;">
            @if($pagination->currentPage() > 2)
                <li class="page-item">
                    <a class="page-link" href="{{route(Route::current()->getName())}}?page=1" aria-label="1">
                        <span aria-hidden="true">1</span>
                    </a>
                </li>
            @endif
            @if($pagination->currentPage() == 1)
                @include('partials.pagination.page_first')
            @elseif(!$pagination->hasMorePages())
                @include('partials.pagination.page_current')
            @else
                @include('partials.pagination.page_last')
            @endif

            @if($pagination->hasMorePages() && $pagination->lastPage() !== ($pagination->currentPage() + 1))
                <li class="page-item">
                    <a class="page-link"
                       href="{{route(Route::current()->getName())}}?page={{$pagination->lastPage()}}{{$query_search}}"
                       aria-label="Next">
                        <span aria-hidden="true">{{$pagination->lastPage()}}</span>
                    </a>
                </li>
            @endif
        </ul>
    </nav>
@endif
