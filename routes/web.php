<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Auth::routes();
Route::get('/logout', [\App\Http\Controllers\Auth\LoginController::class, 'logout']);

Route::group([
    'middleware' => 'authRoleUser'
], function () {
    Route::get('/', [\App\Http\Controllers\Front\BlogController::class, 'index'])->name('index');
    Route::get('/blog/{item}', [\App\Http\Controllers\Front\BlogController::class, 'item'])->name('blog.item');
    Route::match(['get', 'post'],'/profile', [\App\Http\Controllers\Front\ProfileController::class, 'index'])->name('profile');
    Route::get('/profile/delete', [\App\Http\Controllers\Front\ProfileController::class, 'delete'])->name('profile.delete');
});

Route::group([
    'as' => 'admin.',
    'prefix' => 'admin',
    'middleware' => 'authRoleAdmin'
], function () {
    Route::group([
        'as' => 'blog.',
        'prefix' => 'blog',
    ], function () {
        Route::get('/', [App\Http\Controllers\Admin\BlogController::class, 'index'])->name('index');
        Route::match(['get', 'post'], '/form', [App\Http\Controllers\Admin\BlogController::class, 'form'])->name('form');
        Route::match(['get', 'post'], '/form/{id}', [App\Http\Controllers\Admin\BlogController::class, 'form'])->name('id');
        Route::get('/action/{id}/{action}', [App\Http\Controllers\Admin\BlogController::class, 'action'])->name('action');
    });
});
