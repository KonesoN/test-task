<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogItem extends Model
{
    protected $fillable = [
        'title',
        'body',
        'image_url'
    ];
}
