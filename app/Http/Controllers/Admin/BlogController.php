<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BlogItem;
use App\Services\UnsplashService;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $query = BlogItem::orderBy('id', 'DESC');
        return view('admin.blog.index',
            [
                'pagination' => $query->paginate(4, ['*'], 'page', $request->get('page', 1)),
            ]);
    }

    public function form(Request $request, $id = null)
    {
        $item = null;
        if ($request->isMethod('POST')) {
            $request->validate([
                'title' => 'required|max:255',
                'body' => 'required',
            ]);

            if ($id) {
                $item = BlogItem::find($id);
            }
            if ($item) {
                $image_url = $item->image_url;
                if ($request->get('is_generate_new_photo')) {
                    $unsplash_image = UnsplashService::getImage();
                    $image_url = $unsplash_image->urls['small'];
                }
                $item->update(array_merge($request->only(['title', 'body']), ['image_url' => $image_url]));
            } else {
                $unsplash_image = UnsplashService::getImage();
                $image_url = $unsplash_image->urls['small'];
                $item = BlogItem::create(array_merge($request->only(['title', 'body']), ['image_url' => $image_url]));
                $id = $item->id;
            }
            return redirect(route('admin.blog.id', $id));
        }
        if ($id) {
            $item = BlogItem::find($id);
        }
        return view('admin.blog.form', ['item' => $item]);
    }

    public function action(Request $request, $id, $action)
    {
        $item = BlogItem::find($id);
        if ($item) {
            switch ($action) {
                case 'delete':
                    $item->delete();
                    break;
            }
        }
        return redirect(route('admin.blog.index'));
    }
}
