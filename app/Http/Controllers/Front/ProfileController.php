<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{

    public function index(Request $request)
    {
        if ($request->isMethod('POST')) {
            $id = Auth::id();
            $request->validate([
                'email' => "required|max:255|email|unique:users,email,$id",
            ]);
            $user = Auth::user();
            $update_data = [
                'email' => $request->get('email')
            ];
            if ($request->get('password')) {
                $update_data['password'] = Hash::make($request->get('password'));
            }
            if ($file = $request->file('avatar')) {
                if (strpos($file->getMimeType(), 'image/') !== false
                    && in_array($file->getMimeType(),
                        ['image/jpg', 'image/jpeg', 'image/png']) !== false
                ) {
                    $format = str_replace('image/', '', $file->getMimeType());
                    $photo_name = md5(rand(10000000, 99999999) . time());
                    $full_name = $photo_name . '.' . $format;
                    Storage::disk('public')->put("avatar/$full_name", File::get($file));
                    $update_data['avatar'] = '/storage/avatar/' . $full_name;
                }
            }
            $user->update($update_data);
            return redirect(route('profile'));
        }
        return view('front.profile');
    }

    public function delete()
    {
        $user = Auth::user();
        $user->delete();
        Auth::logout();
        return redirect(route('index'));
    }
}
