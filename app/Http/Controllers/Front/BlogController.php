<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Resources\BlogItemResource;
use App\Models\BlogItem;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $query = BlogItem::orderBy('id', 'DESC');
        $items = BlogItemResource::collection($query->paginate(4, ["*"], "page", $request->get('page')));
        return view('front.blog.index', [
            'pagination' => $items
        ]);
    }

    public function item(BlogItem $item)
    {
        return view('front.blog.item', [
            'item' => $item
        ]);
    }
}
