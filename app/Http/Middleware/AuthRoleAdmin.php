<?php

namespace App\Http\Middleware;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthRoleAdmin
{
    public function handle(Request $request, Closure $next, string ...$guards): Response
    {
        $user = Auth::user();
        if ($user) {
            switch ($user->role_id) {
                case User::ROLE_ADMIN:
                    return $next($request);
                    break;
                case User::ROLE_USER:
                    return redirect(route('index'));
                    break;
            }
        }
        return redirect(RouteServiceProvider::LOGIN);
    }
}
