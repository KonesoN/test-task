<?php

namespace App\Services;

use Unsplash;

class UnsplashService
{

    static function getImage()
    {
        Unsplash\HttpClient::init([
            'applicationId' => config('unsplash.application_id', 'CUE3AyJKEXllzXciZZX9Ie57R6a8m66_8Px6vKcvvHg'),
            'secret' => config('unsplash.secret', 'WxDdodLNtEj5XfURiF7GLnc__fAMiX4KQGWYCxB-xcE'),
            'callbackUrl' => 'https://your-application.com/oauth/callback',
            'utmSource' => 'New App'
        ]);
        $filters = [
            'w' => 500,
            'h' => 500
        ];
        return Unsplash\Photo::random($filters);
    }
}
