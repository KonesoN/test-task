Copy .env.example to .env
Configure DB_* and UNSPLASH_* variables
composer install
php artisan migrate
php artisan db:seed
php artisan storage:link
npm i
npm run dev
php artisan serve
