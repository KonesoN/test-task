<?php

namespace Database\Seeders;

use App\Models\BlogItem;
use App\Services\UnsplashService;
use Illuminate\Database\Seeder;

class BlogItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $items = [
            [
                'title' => 'Новость ' . self::getRandomHash(),
                'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Respondent extrema primis, media utrisque, omnia omnibus. At ille non pertimuit saneque fidenter: Istis quidem ipsis verbis, inquit; Utrum igitur tibi litteram videor an totas paginas commovere? Aliter enim explicari, quod quaeritur, non potest. Idemne, quod iucunde? Duo Reges: constructio interrete. Expressa vero in iis aetatibus, quae iam confirmatae sunt. Sic enim censent, oportunitatis esse beate vivere. Ut proverbia non nulla veriora sint quam vestra dogmata. Et nemo nimium beatus est; ' . self::getRandomHash()
            ],
            [
                'title' => 'Новость ' . self::getRandomHash(),
                'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Respondent extrema primis, media utrisque, omnia omnibus. At ille non pertimuit saneque fidenter: Istis quidem ipsis verbis, inquit; Utrum igitur tibi litteram videor an totas paginas commovere? Aliter enim explicari, quod quaeritur, non potest. Idemne, quod iucunde? Duo Reges: constructio interrete. Expressa vero in iis aetatibus, quae iam confirmatae sunt. Sic enim censent, oportunitatis esse beate vivere. Ut proverbia non nulla veriora sint quam vestra dogmata. Et nemo nimium beatus est; ' . self::getRandomHash()
            ],
            [
                'title' => 'Новость ' . self::getRandomHash(),
                'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Respondent extrema primis, media utrisque, omnia omnibus. At ille non pertimuit saneque fidenter: Istis quidem ipsis verbis, inquit; Utrum igitur tibi litteram videor an totas paginas commovere? Aliter enim explicari, quod quaeritur, non potest. Idemne, quod iucunde? Duo Reges: constructio interrete. Expressa vero in iis aetatibus, quae iam confirmatae sunt. Sic enim censent, oportunitatis esse beate vivere. Ut proverbia non nulla veriora sint quam vestra dogmata. Et nemo nimium beatus est; ' . self::getRandomHash()
            ],
            [
                'title' => 'Новость ' . self::getRandomHash(),
                'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Respondent extrema primis, media utrisque, omnia omnibus. At ille non pertimuit saneque fidenter: Istis quidem ipsis verbis, inquit; Utrum igitur tibi litteram videor an totas paginas commovere? Aliter enim explicari, quod quaeritur, non potest. Idemne, quod iucunde? Duo Reges: constructio interrete. Expressa vero in iis aetatibus, quae iam confirmatae sunt. Sic enim censent, oportunitatis esse beate vivere. Ut proverbia non nulla veriora sint quam vestra dogmata. Et nemo nimium beatus est; ' . self::getRandomHash()
            ],
            [
                'title' => 'Новость ' . self::getRandomHash(),
                'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Respondent extrema primis, media utrisque, omnia omnibus. At ille non pertimuit saneque fidenter: Istis quidem ipsis verbis, inquit; Utrum igitur tibi litteram videor an totas paginas commovere? Aliter enim explicari, quod quaeritur, non potest. Idemne, quod iucunde? Duo Reges: constructio interrete. Expressa vero in iis aetatibus, quae iam confirmatae sunt. Sic enim censent, oportunitatis esse beate vivere. Ut proverbia non nulla veriora sint quam vestra dogmata. Et nemo nimium beatus est; ' . self::getRandomHash()
            ],
        ];
        foreach ($items as $item) {
            $unsplash_image = UnsplashService::getImage();
            $image_url = $unsplash_image->urls['small'];
            BlogItem::create(array_merge($item, ['image_url' => $image_url]));
        }
    }

    function getRandomHash()
    {
        return rand(100, 999) . time();
    }
}
