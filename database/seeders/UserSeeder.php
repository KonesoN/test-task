<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $items = [
            [
                'email' => 'admin@ema.il',
                'role_id' => User::ROLE_ADMIN
            ],
            [
                'email' => 'user@ema.il',
                'role_id' => User::ROLE_USER
            ],
        ];
        foreach ($items as $item) {
            User::firstOrCreate(['email' => $item['email']], [
                'email' => $item['email'],
                'password' => Hash::make($item['email']),
                'role_id' => $item['role_id'],
            ]);
        }
    }
}
